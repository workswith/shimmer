// libs
import {provideStore} from '@ngrx/store';

// app
// import {nameListReducer} from './services/name-list.service';
import {Note,noteReducer} from './services/note.service';
import {Notebook,notebookReducer} from './services/notebook.service';
import {MULTILINGUAL_PROVIDERS, MultilingualStateI, multilingualReducer} from '../i18n/index';

// state definition
export interface AppStoreI {
  i18n: MultilingualStateI;
  notebooks: Array<Notebook>;
  notes: Array<Note>;
};

export const APP_PROVIDERS: any[] = [
  MULTILINGUAL_PROVIDERS,
  provideStore({
    i18n: multilingualReducer,
    notebooks: notebookReducer,
    notes: noteReducer
  })
];

// services
export * from './services/app-config.service';
export * from './services/name-list.service';
