import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

// libs
import {Store, ActionReducer, Action} from '@ngrx/store';


export class Notebook {
  _id: string;
  name: string;
}


/**
 * ngrx setup start --
 */
export const NOTEBOOK_ACTIONS: any = {
  INIT: `NOTEBOOK_INIT`,
  NOTEBOOK_ADDED: `NOTEBOOK_ADDED`,
  NOTEBOOK_CHANGED: `NOTEBOOK_CHANGED`,
  NOTEBOOK_DELETED: `NOTEBOOK_DELETED`
};

export const notebookReducer: ActionReducer<any> = (state: any = [], action: Action) => {
  switch (action.type) {
    case NOTEBOOK_ACTIONS.INIT:
      return [...action.payload];
    case NOTEBOOK_ACTIONS.NOTEBOOK_ADDED:
      return [...state, action.payload];
    case NOTEBOOK_ACTIONS.NOTEBOOK_CHANGED:
      let index = state.map(function(e) { return e._id; }).indexOf(action.payload._id);
      if (index >= 0) {
        return [
          ...state.slice(0, index),
          action.payload,
          ...state.slice(index + 1)
        ];
      } else {
        console.log('Error updating note');
        return state;
      }
    case NOTEBOOK_ACTIONS.NOTEBOOK_DELETED:
      let index = state.map(function(e) { return e._id; }).indexOf(action.payload._id);
      return [
        ...state.slice(0, index),
        ...state.slice(index + 1)
      ];
    default:
      return state;
  }
};
/**
 * ngrx end --
 */

@Injectable()
export class NotebookService {
  public notebooks: Observable<Notebook[]>;
  private notebooksUrl: string = 'http://localhost:5554/rest/notebooks';

  constructor(private http: Http,
              private store: Store<any>) {
    this.notebooks = store.select('notebooks');

    this.init();
  }

  init() {
    this.getNotebooks();
  }

  getNotebooks() {
    this.http.get(this.notebooksUrl).map(res => res.json())
      .subscribe((results: any) => {
        this.store.dispatch({ type: NOTEBOOK_ACTIONS.INIT, payload: results });
      });
  }

 add(notebook: Notebook): Promise<Notebook> {
   let headers = new Headers({'Content-Type': 'application/json'});
   this.http
        .post(this.notebooksUrl, notebook, {headers: headers})
        .toPromise()
        .then(res => {
          this.store.dispatch({ type: NOTEBOOK_ACTIONS.NOTEBOOK_ADDED, payload: JSON.parse(res._body) });
        })
        .catch(this.handleError);
 }

update(notebook: Notebook): Promise<Notebook> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.notebooksUrl}/${notebook._id}`;

    this.http
         .put(url, notebook, {headers: headers})
         .toPromise()
         .catch(this.handleError);

    this.store.dispatch({ type: NOTEBOOK_ACTIONS.NOTEBOOK_CHANGED, payload: notebook});
 }

 delete(notebook: Notebook): Promise<Response> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.notebooksUrl}/${notebook._id}`;

    this.store.dispatch({ type: NOTEBOOK_ACTIONS.NOTEBOOK_DELETED, payload: notebook});

    return this.http
             .delete(url, {headers: headers})
             .toPromise()
             .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
