import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

// libs
import {Store, ActionReducer, Action} from '@ngrx/store';


export class Note {
  _id: string;
  notebookId: string;
  name: string;
  note: string;
}

/**
 * ngrx setup start --
 */
export const NOTE_ACTIONS: any = {
  INIT: `NOTE_INIT`,
  NOTE_ADDED: `NOTE_ADDED`,
  NOTE_CHANGED: `NOTE_CHANGED`,
  NOTE_DELETED: `NOTE_DELETED`
};

export const noteReducer: ActionReducer<any> = (state: any = [], action: Action) => {
  switch (action.type) {
    case NOTE_ACTIONS.INIT:
      return [...action.payload];
    case NOTE_ACTIONS.NOTE_ADDED:
      return [...state, action.payload];
    case NOTE_ACTIONS.NOTE_CHANGED:
      let index = state.map(function(e: any) { return e._id; }).indexOf(action.payload._id);
      if (index >= 0) {
        return [
          ...state.slice(0, index),
          action.payload,
          ...state.slice(index + 1)
        ];
      } else {
        console.log('Error updating note');
        return state;
      }
    case NOTE_ACTIONS.NOTE_DELETED:
      let index = state.map(function(e: any) { return e._id; }).indexOf(action.payload._id);
      return [
        ...state.slice(0, index),
        ...state.slice(index + 1)
      ];
    default:
      return state;
  }
};
/**
 * ngrx end --
 */

 @Injectable()
 export class NoteService {
   public notes: Observable<Note[]>;
   private notesUrl: string = 'http://localhost:5554/rest/notes';

   constructor(private http: Http,
               private store: Store<any>) {
     this.notes = store.select('notes');

     this.init();
   }

   init() {
     this.getNotes();
   }

   getNotes() {
     this.http.get(this.notesUrl).map(res => res.json())
       .subscribe((results: any) => {
         this.store.dispatch({ type: NOTE_ACTIONS.INIT, payload: results });
       });
   }

   add(note: Note): Promise<Note> {
     let headers = new Headers({'Content-Type': 'application/json'});
     this.http
          .post(this.notesUrl, note, {headers: headers})
          .toPromise()
          .then(res => {
            this.store.dispatch({ type: NOTE_ACTIONS.NOTE_ADDED, payload: JSON.parse(res._body) });
          })
          .catch(this.handleError);
   }

  update(note: Note): Promise<Note> {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let url = `${this.notesUrl}/${note._id}`;

      this.http
           .put(url, note, {headers: headers})
           .toPromise()
           .catch(this.handleError);

      this.store.dispatch({ type: NOTE_ACTIONS.NOTE_CHANGED, payload: note});
   }

   delete(note: Note): Promise<Response> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.notesUrl}/${note._id}`;

    this.store.dispatch({ type: NOTE_ACTIONS.NOTE_DELETED, payload: note});

    return this.http
               .delete(url, {headers: headers})
               .toPromise()
               .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
    }
 }
