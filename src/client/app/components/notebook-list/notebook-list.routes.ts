import { RouterConfig } from '@angular/router';

import { NotebookListComponent } from './notebook-list.component';

export const NotebookListRoutes: RouterConfig = [
  {
    path: 'notebook-list',
    component: NotebookListComponent
  }
];
