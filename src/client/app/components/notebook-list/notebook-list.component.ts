import {BaseComponent} from '../../frameworks/core/index';
import {OnInit, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';

import {Notebook, NotebookService} from '../../frameworks/app/services/notebook.service';

@BaseComponent({
  moduleId: module.id,
  selector: 'sd-notebook-list',
  templateUrl: 'notebook-list.component.html',
  styleUrls: ['notebook-list.component.css'],
  providers: [NotebookService]
})
export class NotebookListComponent implements OnInit {
  @Output() selectedNotebook = new EventEmitter();
  private currentNotebook: Notebook;
  private newbook: Notebook = {name: ''};
  private notebooks: Notebook[];
  private showNewNotebook: boolean = false;

  constructor(private notebookService: NotebookService,
              private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.getNotebooks();
  }

  getNotebooks(): void {
    this.notebookService.notebooks.subscribe(notebooks => {
      this.notebooks = notebooks;
      this.cd.markForCheck();
    });
  }

  save(notebook: Notebook) {
    this.notebookService.add(notebook);
    this.newbook.name = '';
  }

  select(notebook: Notebook) {
    this.currentNotebook = notebook;
    this.selectedNotebook.emit(notebook);
  }
}
