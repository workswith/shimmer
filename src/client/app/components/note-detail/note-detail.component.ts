import {BaseComponent} from '../../frameworks/core/index';
import {OnInit, OnChanges, Input, Output, EventEmitter} from '@angular/core';
import * as marked from 'marked';
import * as hljs from 'highlight.js';

import {Note, NoteService} from '../../frameworks/app/services/note.service';

@BaseComponent({
  moduleId: module.id,
  selector: 'sd-note-detail',
  templateUrl: 'note-detail.component.html',
  styleUrls: ['note-detail.component.css'],
  providers: [NoteService]
})
export class NoteDetailComponent implements OnInit, OnChanges {
  @Input() note: Note;
  @Input() editing: boolean;

  private editingNote: boolean = false;

  constructor(private noteService: NoteService) {
  }

  ngOnInit() {
    this.editingNote = false;

    if(this.editing) {
      this.editingNote = true;
    }
  }

  ngOnChanges() {
    if (this.editing) {
      this.editingNote = true;
    }
  }

  md(str: string) {
    if (str) {
      // Synchronous highlighting with highlight.js

      return marked(str, {
        highlight: function (code) {
          console.log('Highlighting Code');
          console.log(hljs.highlightAuto(str).value);
          return hljs.highlightAuto(code).value;
        }
      });
    } else {
      return '';
    }
  }

  editNote() {
    this.editingNote = true;
  }

  saveNote(note: Note) {
    // determine whether or not this is a new note...

    if(note._id) {
      this.noteService.update(note);
    } else {
      this.noteService.add(note);
    }
    this.editingNote = false;
  }

  deleteNote(note: Note) {
    this.noteService.delete(note);
    this.editingNote = false;
    this.note = new Note;
  }
}
