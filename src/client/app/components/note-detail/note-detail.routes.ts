
import { RouterConfig } from '@angular/router';

import { NoteDetailComponent } from './note-detail.component';

export const NoteDetailRoutes: RouterConfig = [
  {
    path: 'note-detail',
    component: NoteDetailComponent
  }
];
