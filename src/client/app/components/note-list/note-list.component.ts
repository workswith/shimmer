import {BaseComponent} from '../../frameworks/core/index';
import {OnChanges, Input, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';

import {Note, NoteService} from '../../frameworks/app/services/note.service';

@BaseComponent({
  moduleId: module.id,
  selector: 'sd-note-list',
  templateUrl: 'note-list.component.html',
  styleUrls: ['note-list.component.css'],
  providers: [NoteService]
})
export class NoteListComponent implements OnChanges {
  @Output() selectedNote = new EventEmitter();
  @Output() editingNote = new EventEmitter<boolean>();
  @Input() notebook: any;

  private currentNote: Note;
  private notes: Note[];

  constructor(private noteService: NoteService,
              private cd: ChangeDetectorRef) {}

  add() {
    this.selectedNote.emit({
      notebookId: (this.notebook ? this.notebook._id : 0), // replace 0 with default notebook eventually
      name: '',
      note: ''
    });

    this.editingNote.emit(true);
  }

  select(note: Note) {
    this.currentNote = note;
    this.selectedNote.emit(note);
  }

  ngOnChanges() {
    this.getNotes();
  }

  getNotes() {
    this.noteService.notes.subscribe(notes => {
      this.notes = this.getNotesByNotebook((this.notebook ? this.notebook._id : 0), notes);
      this.cd.markForCheck();
    });
  }

  getNotesByNotebook(id: number, notes: Note[]) {
    return notes.filter((e: any) => e.notebookId === id);
  }
}
