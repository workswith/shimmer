import { RouterConfig } from '@angular/router';

import { NoteListComponent } from './note-list.component';

export const NoteListRoutes: RouterConfig = [
  {
    path: 'note-list',
    component: NoteListComponent
  }
];
