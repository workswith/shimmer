import {provideRouter, RouterConfig} from '@angular/router';

import {NoteDisplayRoutes} from '../note-display/note-display.routes';
import {NotebookListRoutes} from '../notebook-list/notebook-list.routes';
import {HomeRoutes} from '../home/home.routes';

export const routes: RouterConfig = [
  ...HomeRoutes,
  ...NoteDisplayRoutes,
  ...NotebookListRoutes
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
