import {BaseComponent} from '../../frameworks/core/index';

import {Output} from '@angular/core';

import {NotebookListComponent} from '../notebook-list/notebook-list.component';
import {NoteListComponent} from '../note-list/note-list.component';
import {NoteDetailComponent} from '../note-detail/note-detail.component';

import {Note} from '../../frameworks/app/services/note.service';
import {Notebook} from '../../frameworks/app/services/notebook.service';


@BaseComponent({
  moduleId: module.id,
  selector: 'sd-note-display',
  templateUrl: 'note-display.component.html',
  styleUrls: ['note-display.component.css'],
  directives: [NotebookListComponent, NoteListComponent, NoteDetailComponent]
})
export class NoteDisplayComponent  {
  @Output() note: Note;
  @Output() notebook: Notebook;
  @Output() editing: boolean;

  editingNote(edit: boolean) {
    this.editing = edit;
  }

  selectedNotebook(notebook: Notebook) {
    this.notebook = notebook;
  }

  selectedNote(note: Note) {
    this.note = note;
  }

  savedNote(note: Note) {
  }
}
