import { RouterConfig } from '@angular/router';

import { NoteDisplayComponent } from './note-display.component';

export const NoteDisplayRoutes: RouterConfig = [
  {
    path: '',
    component: NoteDisplayComponent
  }
];
